package com.agiletestingalliance;

public class MinMax {

    public int funcReturnMax(int alpha, int beta) {
        if (beta > alpha) {
            return beta;
        }
        else {
            return alpha;
        }
    }

}
